package de.nurmarvin.discordbot.utils;

import lombok.Data;
import lombok.NonNull;
import net.dv8tion.jda.core.entities.Message;

@Data
public class CachedMessage {
    @NonNull
    private Message originalMessage;
    private Message latestEdit;

    public boolean isEdited() {
        return latestEdit != null;
    }
}
