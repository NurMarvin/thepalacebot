package de.nurmarvin.discordbot.utils;

import de.nurmarvin.discordbot.ThePalaceBot;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Role;

public class PermissionHelper {
    public static boolean isAdmin(Member member) {
        if(member.isOwner()) return true;
        for(Role roles : member.getRoles())
            if(ThePalaceBot.getInstance().getConfig().getAdminRoles().contains(roles.getIdLong()))
                return true;
        return false;
    }

    public static boolean isMod(Member member) {
        if(isAdmin(member)) return true;
        for(Role roles : member.getRoles())
            if(ThePalaceBot.getInstance().getConfig().getModRoles().contains(roles.getIdLong()))
                return true;
        return false;
    }

    public static boolean canBypass(Member member) {
        return ThePalaceBot.getInstance().getConfig().getAdminBypass() && isAdmin(member);
    }
}
