package de.nurmarvin.discordbot.utils;

import java.util.regex.Pattern;

public class LinkHelper {
    private static final String URL_REGEX = "^(http://www\\.|https://www\\.|http://|https://)" +
                                            "?[a-z0-9]+([\\-.][a-z0-9]+)*\\.[a-z]{2,5}(:[0-9]{1,5})?(/.*)?$";
    public static boolean containsLink(String message) {
        return Pattern.matches(URL_REGEX, message);
    }
}
