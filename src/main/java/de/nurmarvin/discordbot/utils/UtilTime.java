package de.nurmarvin.discordbot.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class UtilTime {
    public static final String DATE_FORMAT_NOW = "dd.MM.yyyy HH:mm:ss";
    public static final String DATE_FORMAT_DAY = "dd.MM.yyyy";
    public static final String TIME_FORMAT_NOW = "HH:mm";

    public static String now() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
        return sdf.format(cal.getTime());
    }

    public static String when(long time) {
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
        return sdf.format(time);
    }


    public static String date() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_DAY);
        return sdf.format(cal.getTime());
    }

    public static String time() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat(TIME_FORMAT_NOW);
        return sdf.format(cal.getTime());
    }

    public static String since(long epoch) {
        return "Took " + convertString(System.currentTimeMillis() - epoch, 1, TimeUnit.FIT) + ".";
    }

    public static double convert(long time, int trim, TimeUnit type) {
        type = getTimeUnit(time, type);

        if (type == TimeUnit.DAYS) return UtilMath.trim(trim, (time) / 86400000d);
        if (type == TimeUnit.HOURS) return UtilMath.trim(trim, (time) / 3600000d);
        if (type == TimeUnit.MINUTES) return UtilMath.trim(trim, (time) / 60000d);
        if (type == TimeUnit.SECONDS) return UtilMath.trim(trim, (time) / 1000d);
        else return UtilMath.trim(trim, time);
    }

    private static TimeUnit getTimeUnit(long time, TimeUnit type) {
        if (type == TimeUnit.FIT) {
            if (time < 60000) type = TimeUnit.SECONDS;
            else if (time < 3600000) type = TimeUnit.MINUTES;
            else if (time < 86400000) type = TimeUnit.HOURS;
            else type = TimeUnit.DAYS;
        }
        return type;
    }

    public static String MakeStr(long time) {
        return convertString(time, 1, TimeUnit.FIT);
    }

    public static String MakeStr(long time, int trim) {
        return convertString(Math.max(0, time), trim, TimeUnit.FIT);
    }

    public static String convertString(long time, int trim, TimeUnit type) {
        if (time == -1) return "Permanent";

        type = getTimeUnit(time, type);

        String text;
        double num;
        if (trim == 0) {
            if (type == TimeUnit.DAYS) text = (num = UtilMath.trim(trim, time / 86400000d)) + " Day";
            else if (type == TimeUnit.HOURS) text = (num = UtilMath.trim(trim, time / 3600000d)) + " Hour";
            else if (type == TimeUnit.MINUTES) text = (num = UtilMath.trim(trim, time / 60000d)) + " Minute";
            else if (type == TimeUnit.SECONDS) text = (int) (num = (int) UtilMath.trim(trim, time / 1000d)) + " " +
                                                      "Second";
            else text = (int) (num = (int) UtilMath.trim(trim, time)) + " Millisecond";
        } else {
            if (type == TimeUnit.DAYS) text = (num = UtilMath.trim(trim, time / 86400000d)) + " Day";
            else if (type == TimeUnit.HOURS) text = (num = UtilMath.trim(trim, time / 3600000d)) + " Hour";
            else if (type == TimeUnit.MINUTES) text = (num = UtilMath.trim(trim, time / 60000d)) + " Minute";
            else if (type == TimeUnit.SECONDS) text = (num = UtilMath.trim(trim, time / 1000d)) + " Second";
            else text = (int) (num = (int) UtilMath.trim(0, time)) + " Millisecond";
        }

        if (num != 1)
            text += "s";

        return text;
    }

    public static String convertSmallString(long time, int trim, TimeUnit type) {
        if (time == -1) return "Infinite";

        type = getTimeUnit(time, type);

        String text;
        if (trim == 0) {
            if (type == TimeUnit.DAYS) text = UtilMath.trim(trim, time / 86400000d) + "d";
            else if (type == TimeUnit.HOURS) text = UtilMath.trim(trim, time / 3600000d) + "h";
            else if (type == TimeUnit.MINUTES) text = UtilMath.trim(trim, time / 60000d) + "m";
            else if (type == TimeUnit.SECONDS) text = (int) UtilMath.trim(trim, time / 1000d) + " " +
                                                      "s";
            else text = (int) UtilMath.trim(trim, time) + "ms";
        } else {
            if (type == TimeUnit.DAYS) text = UtilMath.trim(trim, time / 86400000d) + "d";
            else if (type == TimeUnit.HOURS) text = UtilMath.trim(trim, time / 3600000d) + "h";
            else if (type == TimeUnit.MINUTES) text = UtilMath.trim(trim, time / 60000d) + "m";
            else if (type == TimeUnit.SECONDS) text = UtilMath.trim(trim, time / 1000d) + "s";
            else text = (int) UtilMath.trim(0, time) + "ms";
        }

        return text;
    }

    public static String makeItFit(int time) {
        String string = String.valueOf(time);

        if (string.length() < 2)
            string = "0" + string;
        return string;
    }

    public static boolean elapsed(long from, long required) {
        return System.currentTimeMillis() - from > required;
    }

    public enum TimeUnit {
        FIT,
        DAYS,
        HOURS,
        MINUTES,
        SECONDS,
        MILLISECONDS
    }
}
