package de.nurmarvin.discordbot.utils;

import de.nurmarvin.discordbot.ThePalaceBot;
import de.nurmarvin.discordbot.utils.throwables.InvalidIdException;
import de.nurmarvin.discordbot.utils.throwables.InvalidMentionException;
import de.nurmarvin.discordbot.utils.throwables.NotAMentionException;
import net.dv8tion.jda.core.entities.*;

import java.util.regex.Pattern;

public class MentionHelper {
    private static final String CHANNEL_REGEX = "<#(\\d{18})>";
    private static final String USER_REGEX = "<@!(\\d{18})>";
    private static final String MEMBER_REGEX = "<@(\\d{18})>";

    public static TextChannel getChannelFromMention(String mention) throws InvalidMentionException,
                                                                           NotAMentionException {
        long id;
        if(Pattern.matches(CHANNEL_REGEX, mention)) {
            id = Long.parseLong(mention.substring(2, mention.length() -1));
            if (ThePalaceBot.getInstance().getJda().getTextChannelById(id) == null) throw new InvalidMentionException();
        }
        else throw new NotAMentionException();

        return ThePalaceBot.getInstance().getJda().getTextChannelById(id);
    }

    public static User getUserFromMention(String mention) throws InvalidMentionException,
                                                                 NotAMentionException {
        long id;
        if(Pattern.matches(mention.contains("!") ? USER_REGEX : MEMBER_REGEX, mention)) {
            id = Long.parseLong(mention.substring(mention.contains("!") ? 3 : 2, mention.length() -1));
            if (ThePalaceBot.getInstance().getJda().getUserById(id) == null) throw new InvalidMentionException();
        }
        else throw new NotAMentionException();

        return ThePalaceBot.getInstance().getJda().getUserById(id);
    }

    public static Member getMemberFromMention(Guild guild, String mention) throws InvalidMentionException,
                                                                                  NotAMentionException {
        long id;
        if(Pattern.matches(mention.contains("!") ? USER_REGEX : MEMBER_REGEX, mention)) {
            id = Long.parseLong(mention.substring(mention.contains("!") ? 3 : 2, mention.length() -1));
            if (guild.getMemberById(id) == null) throw new InvalidMentionException();
        }
        else throw new NotAMentionException();

        return guild.getMemberById(id);
    }

    public static Long getId(Guild guild, String message) {
        long id;

        try {
            id = MentionHelper.getMemberFromMention(guild, message).getUser().getIdLong();
        } catch (InvalidMentionException | NotAMentionException e) {
            try {
                id = Long.parseLong(message);
                if (ThePalaceBot.getInstance().getJda().getUserById(id) == null) throw new InvalidIdException();
            } catch (Exception e1) {
                id = -1L;
            }
        }
        return id;
    }
}
