package de.nurmarvin.discordbot.utils;

import net.dv8tion.jda.core.entities.Message;

public class AttachmentHelper {
    public static boolean hasImageAttached(Message message) {
        if(!hasAttachment(message)) return false;

        for (Message.Attachment attachment : message.getAttachments())
            if(attachment.isImage())
                return true;
        return false;
    }

    public static boolean hasAttachment(Message message) {
        return message.getAttachments() != null && message.getAttachments().size() > 0;
    }
}
