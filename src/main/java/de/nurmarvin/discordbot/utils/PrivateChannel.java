package de.nurmarvin.discordbot.utils;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;

@Data @AllArgsConstructor @NoArgsConstructor
public class PrivateChannel {
    private Long channelId;
    private Long ownerUserId;
    private ArrayList<Long> usersWithAccess;
}
