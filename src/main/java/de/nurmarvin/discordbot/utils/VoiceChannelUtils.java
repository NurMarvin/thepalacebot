package de.nurmarvin.discordbot.utils;

import de.nurmarvin.discordbot.ThePalaceBot;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.VoiceChannel;

public class VoiceChannelUtils {

    public static void kick(Member member) {
        if(!member.getVoiceState().inVoiceChannel()) return;
        member.getGuild().getController().createVoiceChannel("Kick").queue(c -> {
            c.getManager().setParent(c.getGuild().getCategoryById(ThePalaceBot.getInstance().getConfig().getSpecialCategoryId())).queue(done -> {
                c.getGuild().getController().moveVoiceMember(member, (VoiceChannel) c).queue(aVoid -> c.delete().queue());
            });
        });
    }
}
