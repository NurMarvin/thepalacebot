package de.nurmarvin.discordbot.utils;

import com.google.common.collect.Lists;
import de.nurmarvin.discordbot.ThePalaceBot;
import de.nurmarvin.discordbot.utils.enums.SpecialChannelPermissions;

import java.util.ArrayList;
import java.util.Arrays;

public class SpecialChannelUtils {
    public static void addSpecialPermissions(long channelId, SpecialChannelPermissions... permissions) {
        ArrayList<SpecialChannelPermissions> channelPermissions =
                ThePalaceBot.getInstance().getConfig().getSpecialChannelPermissions().getOrDefault(channelId, Lists.newArrayList());

        channelPermissions.addAll(Arrays.asList(permissions));
        ThePalaceBot.getInstance().getConfig().getSpecialChannelPermissions().put(channelId, channelPermissions);
    }

    public static void removeSpecialPermissions(long channelId, SpecialChannelPermissions... permissions) {
        ArrayList<SpecialChannelPermissions> channelPermissions =
                ThePalaceBot.getInstance().getConfig().getSpecialChannelPermissions().getOrDefault(channelId, Lists.newArrayList());

        channelPermissions.removeAll(Arrays.asList(permissions));
        ThePalaceBot.getInstance().getConfig().getSpecialChannelPermissions().put(channelId, channelPermissions);
    }

    public static ArrayList<SpecialChannelPermissions> getSpecialPermissions(long channelId) {
        return ThePalaceBot.getInstance().getConfig().getSpecialChannelPermissions().getOrDefault(channelId, Lists.newArrayList());
    }
}
