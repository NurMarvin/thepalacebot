package de.nurmarvin.discordbot.utils.throwables;

public class InvalidIdException extends Exception {
    public InvalidIdException() {
        super("The given ID can not be assigned to a user");
    }
}
