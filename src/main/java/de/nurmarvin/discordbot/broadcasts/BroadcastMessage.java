package de.nurmarvin.discordbot.broadcasts;

import de.nurmarvin.discordbot.ThePalaceBot;
import lombok.AllArgsConstructor;
import lombok.Data;
import net.dv8tion.jda.core.EmbedBuilder;

import java.util.UUID;

@AllArgsConstructor @Data
public class BroadcastMessage implements Runnable {

    private UUID uuid;
    private String message;
    private long guildId;
    private long channelId;
    private long delayInMillis;

    @Override
    public void run() {
        EmbedBuilder embedBuilder = ThePalaceBot.getInstance().getEmbedBase();

        embedBuilder.setTitle("Automated message");
        embedBuilder.appendDescription(message);

        ThePalaceBot.getInstance().getJda().getGuildById(guildId).getTextChannelById(channelId).sendMessage(embedBuilder.build()).queue();
    }
}
