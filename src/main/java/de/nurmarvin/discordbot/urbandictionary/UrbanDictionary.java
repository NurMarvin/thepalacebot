package de.nurmarvin.discordbot.urbandictionary;

import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import de.nurmarvin.discordbot.ThePalaceBot;
import lombok.Data;
import org.apache.http.client.fluent.Request;

import java.io.IOException;
import java.util.ArrayList;

public class UrbanDictionary {
    private static final String LOOKUP_URL = "http://api.urbandictionary.com/v0/define?term=%s";

    public static ArrayList<Entry> lookUp(String term) throws IOException {
        JsonObject jsonObject = ThePalaceBot.getInstance().getGson().fromJson(Request.Get(String.format(LOOKUP_URL,
                                                                                                        term.replace(" ", "%20")))
                                                                                     .execute().returnContent().asString(), JsonObject.class);

        return ThePalaceBot.getInstance().getGson().fromJson(jsonObject.get("list").getAsJsonArray().toString(),
                                                             new TypeToken<ArrayList<Entry>>(){}.getType());
    }

    @Data
    public class Entry {
        private String definition;
        private String permalink;
        private int thumbs_up;
        private int thumbs_down;
        private String author;
        private int defid;
        private String current_vote;
        private String written_on;
        private String example;
    }
}
