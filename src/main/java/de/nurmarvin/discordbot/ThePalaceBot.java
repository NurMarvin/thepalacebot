package de.nurmarvin.discordbot;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import de.nurmarvin.discordbot.broadcasts.BroadcastManager;
import de.nurmarvin.discordbot.command.CommandRegistry;
import de.nurmarvin.discordbot.command.commands.*;
import de.nurmarvin.discordbot.handler.*;
import io.sentry.Sentry;
import lombok.Getter;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.entities.MessageEmbed;
import net.dv8tion.jda.core.entities.TextChannel;
import spark.Redirect;

import javax.security.auth.login.LoginException;
import java.io.*;
import java.time.Instant;
import java.util.concurrent.TimeUnit;

import static spark.Spark.*;

@Getter
public final class ThePalaceBot {
    @Getter
    private static ThePalaceBot instance;
    private final Gson gson;
    private final File configFile;
    private Config config;
    private JDA jda;
    private CommandRegistry commandRegistry;
    private BroadcastManager broadcastManager;
    private PrivateChannelHandler privateChannelHandler;
    private MessageHandler messageHandler;

    private ThePalaceBot() {
        this.gson = new GsonBuilder().setPrettyPrinting().create();
        this.configFile = new File("config.json");
    }

    public static void main(String[] args) {
        instance = new ThePalaceBot();
        try {
            instance.init();
        } catch (IOException | LoginException e) {
            Sentry.capture(e);
        }
    }

    private void handleWebRequests() {
        port(8080);

        staticFiles.location("/public");
        get("/", (req, res) -> String.format("Operating the %s Bot. Login at /login",
                                             this.getJda().getSelfUser().getName()));


        notFound((request, response) -> {
            String redirectTo = request.contextPath() + (request.contextPath().endsWith("/") ? "" : "/") + "index.html";
            redirect.any(request.contextPath(), redirectTo, Redirect.Status.MOVED_PERMANENTLY);
            return null;
        });
    }

    public void init() throws IOException, LoginException {
        if (this.configFile.exists())
            this.loadConfig();
        else {
            this.config = new Config("token", "sentryDsn", "!", 0L, 0L, 0L, 0L, 0L, 0L, 0L, 0L, Lists.newArrayList(),
                                     Lists.newArrayList(), true, Maps.newHashMap(), 0L, 0L, 0L, 0L, 0L, 0L, 0L, 0L,
                                     0L, 0L, 0L, 0L);
            this.saveConfig();
            System.out.println("Created config. Please insert discord token.");
            System.exit(0);
        }

        Sentry.init(this.config.getSentryDsn());

        this.commandRegistry = new CommandRegistry();

        this.commandRegistry.registerCommand(new AddBotAdminCommand());
        this.commandRegistry.registerCommand(new RemoveBotAdminCommand());
        this.commandRegistry.registerCommand(new AddSpecialChannelPermissionCommand());
        this.commandRegistry.registerCommand(new HelpCommand());
        this.commandRegistry.registerCommand(new RemoveSpecialChannelPermissionCommand());
        this.commandRegistry.registerCommand(new ToggleAdminBypassCommand());
        this.commandRegistry.registerCommand(new SetPrefixCommand());
        this.commandRegistry.registerCommand(new KickCommand());
        this.commandRegistry.registerCommand(new SetNameCommand());
        this.commandRegistry.registerCommand(new UpdateAvatarCommand());
        this.commandRegistry.registerCommand(new PingCommand());
        this.commandRegistry.registerCommand(new BanCommand());
        this.commandRegistry.registerCommand(new AddBroadcastMessageCommand());
        this.commandRegistry.registerCommand(new ListBroadcastMessagesCommand());
        this.commandRegistry.registerCommand(new RemoveBroadcastMessageCommand());
        this.commandRegistry.registerCommand(new UrbanDictionaryCommand());
        this.commandRegistry.registerCommand(new EightballCommand());
        this.commandRegistry.registerCommand(new ReportCommand());
        this.commandRegistry.registerCommand(new AllowCommand());
        this.commandRegistry.registerCommand(new DisallowCommand());
        this.commandRegistry.registerCommand(new RankCommand());
        this.commandRegistry.registerCommand(new AddBotModCommand());
        this.commandRegistry.registerCommand(new ThrowErrorCommand());

        this.broadcastManager = new BroadcastManager();
        this.broadcastManager.startSchedulers();

        //this.handleWebRequests();

        this.jda = new JDABuilder(this.config.getToken())
                .addEventListener(this.commandRegistry)
                .addEventListener(new SpecialChannelHandler())
                .addEventListener(this.messageHandler = new MessageHandler())
                .addEventListener(new VerificationHandler())
                .addEventListener(this.privateChannelHandler = new PrivateChannelHandler())
                .addEventListener(new PublicChannelHandler())
                .addEventListener(new LogHandler())
                .build();

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            try {
                this.saveConfig();
                this.broadcastManager.save();
                this.privateChannelHandler.save();
                this.messageHandler.save();

            } catch (IOException e) {
                Sentry.capture(e);
                e.printStackTrace();
            }
        }));
    }

    public EmbedBuilder getEmbedBase() {
        return new EmbedBuilder().setFooter(this.getJda().getSelfUser().getName() + " Bot by NurMarvin#1337",
                                            this.jda.getSelfUser().getAvatarUrl());
    }

    public EmbedBuilder getLogBase() {
        return this.getEmbedBase().setTitle("Log Message").setTimestamp(Instant.now()).setColor(0xFFca0c00);
    }

    public EmbedBuilder getDebugLogBase() {
        return this.getEmbedBase().setTitle("Log Message").setTimestamp(Instant.now()).setColor(0xFF00a9fb);
    }

    public void saveConfig() throws IOException {
        FileWriter fileWriter = new FileWriter(this.configFile);
        fileWriter.write(this.gson.toJson(this.config));
        fileWriter.close();
    }

    public void loadConfig() throws FileNotFoundException {
        this.config = this.gson.fromJson(new FileReader(this.configFile), Config.class);
    }

    public void sendAutoDeleteMessage(MessageChannel channel, MessageEmbed message, long seconds) {
        channel.sendMessage(message).queue(m -> m.delete().queueAfter(seconds, TimeUnit.SECONDS, success -> {
            EmbedBuilder embedBuilder = this.getLogBase().setColor(0xFF00a9fb);

            embedBuilder.addField("Action", "Auto Message", false);
            embedBuilder.addField("Channel", m.getTextChannel().getAsMention(), false);
            embedBuilder.addField("Deletion Delay (in seconds)", String.valueOf(seconds), false);

            this.debugLog(m.getGuild(), embedBuilder.build());
        }, Sentry::capture));
    }

    public void log(Guild guild, MessageEmbed message) {
        TextChannel logChannel = guild.getTextChannelById(this.getConfig().getLogChannelId());
        logChannel.sendMessage(message).queue();
    }

    public void debugLog(Guild guild, MessageEmbed message) {
        TextChannel logChannel = guild.getTextChannelById(this.getConfig().getDebugLogChannelId());
        logChannel.sendMessage(message).queue();
    }
}
