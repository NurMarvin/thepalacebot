package de.nurmarvin.discordbot.command;

import com.google.common.collect.Lists;
import de.nurmarvin.discordbot.ThePalaceBot;
import lombok.Getter;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class CommandRegistry extends ListenerAdapter {
    @Getter
    private ArrayList<Command> commands;

    public CommandRegistry() {
        this.commands = Lists.newArrayList();
    }

    public void registerCommand(Command command) {
        this.commands.add(command);
    }

    public Command getCommand(String name)
    {
        for(Command command : this.commands)
            if(command.getName().equalsIgnoreCase(name))return command;
            else for (String alias : command.getAliases()) if(name.equalsIgnoreCase(alias)) return command;
        return null;
    }

    public boolean handleCommand(GuildMessageReceivedEvent event) {
        String prefix = ThePalaceBot.getInstance().getConfig().getPrefix();

        if(!event.getMessage().getContentRaw().startsWith(prefix))
            return false;

        String commandName = event.getMessage().getContentRaw().substring(prefix.length());
        String[] args = null;

        if (commandName.contains(" "))
        {
            commandName = commandName.split(" ")[0];
            args = event.getMessage().getContentRaw().substring(event.getMessage().getContentRaw().indexOf(' ') + 1).split(" ");
        }

        if(args == null)
        {
            args = new String[0];
        }

        Command command = this.getCommand(commandName);

        if(command == null)
            return true;

        long userId = event.getAuthor().getIdLong();

        if(command.isDev() && userId != 199127069240590336L)
        {
            EmbedBuilder embedBuilder = ThePalaceBot.getInstance().getEmbedBase();
            ThePalaceBot.getInstance().sendAutoDeleteMessage(event.getChannel(), embedBuilder.appendDescription(
                    String.format("This command may only be run by my owner, %s.",
                                  event.getAuthor().getAsMention())).build(), 5);
            return true;
        }

        command.execute(args, event.getMessage());
        return true;
    }
}
