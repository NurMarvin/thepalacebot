package de.nurmarvin.discordbot.command.commands;

import de.nurmarvin.discordbot.ThePalaceBot;
import de.nurmarvin.discordbot.command.Command;
import de.nurmarvin.discordbot.utils.UserInfo;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Message;

public class RankCommand extends Command {
    public RankCommand() {
        super("rank", new String[0], "Shows you your or someone else's current level", false);
    }

    @Override
    public void execute(String[] args, Message msg) {
        msg.delete().queue();

        EmbedBuilder embedBuilder = ThePalaceBot.getInstance().getEmbedBase();
        UserInfo userInfo = ThePalaceBot.getInstance().getMessageHandler().getUserInfo(msg.getAuthor().getIdLong());

        embedBuilder.setTitle(String.format("%s#%s's Rank",
                                            msg.getAuthor().getName(), msg.getAuthor().getDiscriminator()));
        embedBuilder.appendDescription(String.format("Your current level is: %s\n", userInfo.getLevel()));
        embedBuilder.appendDescription(String.format("Your progress to the next level is: %s / %s XP",
                                                     userInfo.getCurrentXp(),
                                                     userInfo.getXpRequiredForNextLevel()));

        this.sendAutoDeleteMessage(msg.getChannel(), embedBuilder.build(), 15);
    }
}
