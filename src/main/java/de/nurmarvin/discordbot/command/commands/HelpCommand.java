package de.nurmarvin.discordbot.command.commands;

import de.nurmarvin.discordbot.ThePalaceBot;
import de.nurmarvin.discordbot.command.Command;
import de.nurmarvin.discordbot.utils.PermissionHelper;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Message;

public class HelpCommand extends Command {
    public HelpCommand() {
        super("help", new String[0], "Shows this help page", false);
    }

    @Override
    public void execute(String[] args, Message msg) {
        msg.delete().queue();
        EmbedBuilder embedBuilder = ThePalaceBot.getInstance().getEmbedBase();

        if (!PermissionHelper.isAdmin(msg.getMember())) {
            this.sendAutoDeleteMessage(msg.getChannel(), embedBuilder.appendDescription(
                    String.format("You don't seem to have permissions to do this, %s.", msg.getAuthor().getAsMention()))
                                                                     .build());
            return;
        }

        embedBuilder.appendDescription(String.format("The prefix for every command is `%s`.\n" +
                                                     "This help page will be auto deleted in 15 seconds.",
                                                     ThePalaceBot.getInstance().getConfig().getPrefix()));

        for (Command command : ThePalaceBot.getInstance().getCommandRegistry().getCommands())
            if(!command.equals(this))
                embedBuilder.addField(command.getName(), command.getDescription(), false);

        this.sendAutoDeleteMessage(msg.getChannel(), embedBuilder.build(), 15);
    }
}
