package de.nurmarvin.discordbot.command.commands;

import de.nurmarvin.discordbot.ThePalaceBot;
import de.nurmarvin.discordbot.command.Command;
import de.nurmarvin.discordbot.utils.throwables.InvalidMentionException;
import de.nurmarvin.discordbot.utils.MentionHelper;
import de.nurmarvin.discordbot.utils.throwables.NotAMentionException;
import io.sentry.Sentry;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Message;

public class AllowCommand extends Command {
    public AllowCommand() {
        super("allow", new String[0], "Allow a user to access your private voice channel", false);
    }

    @Override
    public void execute(String[] args, Message msg) {
        msg.delete().queue();
        EmbedBuilder embedBuilder = ThePalaceBot.getInstance().getEmbedBase();

        if(!ThePalaceBot.getInstance().getPrivateChannelHandler().hasPrivateChannel(msg.getAuthor().getIdLong())) {
            this.sendAutoDeleteMessage(msg.getChannel(),
                                       embedBuilder.appendDescription("You don't have a private channel.").build());
        }

        if(args.length < 1) {
            this.sendAutoDeleteMessage(msg.getChannel(),
                                       embedBuilder.appendDescription("Please mention the user you want to give " +
                                                                      "access.").build());
            return;
        }

        Member member = null;
        try {
            member = MentionHelper.getMemberFromMention(msg.getGuild(), args[0]);
        } catch (InvalidMentionException | NotAMentionException e) {
            Sentry.capture(e);
        }

        if(member == null) {
            this.sendAutoDeleteMessage(msg.getChannel(),
                                       embedBuilder.appendDescription("Please mention the user you want to give " +
                                                                      "access.").build());
            return;
        }

        ThePalaceBot.getInstance().getPrivateChannelHandler().addMember(msg.getAuthor().getIdLong(), member);
        this.sendAutoDeleteMessage(msg.getChannel(),
                                   embedBuilder.appendDescription(
                                           String.format("Gave %s access to your private voice channel.",
                                                         member.getAsMention())).build());
    }
}
