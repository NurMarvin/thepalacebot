package de.nurmarvin.discordbot.command.commands;

import de.nurmarvin.discordbot.ThePalaceBot;
import de.nurmarvin.discordbot.command.Command;
import io.sentry.Sentry;
import net.dv8tion.jda.core.entities.Message;

public class ThrowErrorCommand extends Command {
    public ThrowErrorCommand() {
        super("throw", new String[] {"error", "throwerror"}, "Throws an error", true);
    }

    @Override
    public void execute(String[] args, Message msg) {
        msg.delete().queue();
        try {
            this.sendAutoDeleteMessage(msg.getChannel(), ThePalaceBot.getInstance().getEmbedBase().appendDescription(
                    "Exception thrown!").build(), 5);
            throw new NullPointerException("Test NullPointerException");
        } catch (NullPointerException e) {
            Sentry.capture(e);
        }
    }
}
