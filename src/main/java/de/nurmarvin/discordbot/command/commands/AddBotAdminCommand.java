package de.nurmarvin.discordbot.command.commands;

import de.nurmarvin.discordbot.ThePalaceBot;
import de.nurmarvin.discordbot.command.Command;
import de.nurmarvin.discordbot.utils.PermissionHelper;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Message;

import java.io.IOException;
import java.util.Arrays;

public class AddBotAdminCommand extends Command {
    public AddBotAdminCommand() {
        super("addbotadmin", new String[0], "Add a role to be a bot admin", false);
    }

    @Override
    public void execute(String[] args, Message msg) {
        msg.delete().queue();
        EmbedBuilder embedBuilder = ThePalaceBot.getInstance().getEmbedBase();

        if (!PermissionHelper.isAdmin(msg.getMember())) {
            this.sendAutoDeleteMessage(msg.getChannel(), embedBuilder.appendDescription(
                    String.format("You don't seem to have permissions to do this, %s.", msg.getAuthor().getAsMention()))
                                                                     .build());
            return;
        }

        if (args.length == 0) {
            this.sendAutoDeleteMessage(msg.getChannel(),
                                       embedBuilder.appendDescription("Please add the roles's id as an argument")
                                                   .build());
            return;
        }

        long id;

        try {
            id = Long.parseLong(args[0]);
            if (ThePalaceBot.getInstance().getJda().getRoleById(id) == null) throw new Exception();
        } catch (Exception e) {
            this.sendAutoDeleteMessage(msg.getChannel(),
                                       embedBuilder.appendDescription("Role ID must be a valid number.").build());
            return;
        }

        if(ThePalaceBot.getInstance().getConfig().getAdminRoles().contains(id))
        {
            this.sendAutoDeleteMessage(msg.getChannel(),
                                       embedBuilder.appendDescription("This role is already a bot admin.").build());
            return;
        }

        ThePalaceBot.getInstance().getConfig().getAdminRoles().add(id);
        this.sendAutoDeleteMessage(msg.getChannel(),
                                   embedBuilder.appendDescription("Role was added as a bot admin.").build());

        try {
            ThePalaceBot.getInstance().saveConfig();
        } catch (IOException e) {
            this.sendAutoDeleteMessage(msg.getChannel(),
                                       embedBuilder.appendDescription("Error:" + Arrays.toString(e.getStackTrace())).build());
        }
    }
}
