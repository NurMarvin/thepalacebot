package de.nurmarvin.discordbot.command.commands;

import de.nurmarvin.discordbot.ThePalaceBot;
import de.nurmarvin.discordbot.command.Command;
import de.nurmarvin.discordbot.utils.*;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Message;

import java.time.Instant;

public class KickCommand extends Command {
    public KickCommand() {
        super("kick", new String[0], "Kicks a member", false);
    }

    @Override
    public void execute(String[] args, Message msg) {
        msg.delete().queue();
        EmbedBuilder embedBuilder = ThePalaceBot.getInstance().getEmbedBase();

        if (!PermissionHelper.isMod(msg.getMember())) {
            this.sendAutoDeleteMessage(msg.getChannel(), embedBuilder.appendDescription(
                    String.format("You don't seem to have permissions to do this, %s.", msg.getAuthor().getAsMention()))
                                                                     .build());
            return;
        }

        if (args.length > 0) {
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 1; i < args.length; i++) {
                if (i != 1) stringBuilder.append(" ");
                stringBuilder.append(args[i]);
            }

            String reason = stringBuilder.toString();
            long id = MentionHelper.getId(msg.getGuild(), args[0]);

            Member member = msg.getGuild().getMemberById(id);

            this.sendAutoDeleteMessage(msg.getChannel(),
                                       embedBuilder.appendDescription(
                                               String.format("Kicked %s with reason `%s`",
                                                             member == null ? id : member.getAsMention(), reason)).build());

            if(member != null) member.getUser().openPrivateChannel().queue(channel -> {
                EmbedBuilder kickMessage = new EmbedBuilder();

                kickMessage.setTitle("You have been kicked");

                kickMessage.addField("Server", msg.getGuild().getName(), false);
                kickMessage.addField("Reason", reason, false);
                kickMessage.setTimestamp(Instant.now());

                channel.sendMessage(kickMessage.build()).queue();

                this.kick(msg.getGuild(), id, reason);
            }, error -> this.kick(msg.getGuild(), id, reason));
            else this.kick(msg.getGuild(), id, reason);
        } else {
            this.sendAutoDeleteMessage(msg.getChannel(), embedBuilder.appendDescription("Please mention the user you " +
                                                                                        "want to kick").build());
        }
    }

    private void kick(Guild guild, long id, String reason) {
        guild.getController().kick(String.valueOf(id), reason).queue();
    }
}