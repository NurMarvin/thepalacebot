package de.nurmarvin.discordbot.command.commands;

import de.nurmarvin.discordbot.ThePalaceBot;
import de.nurmarvin.discordbot.command.Command;
import de.nurmarvin.discordbot.utils.*;
import de.nurmarvin.discordbot.utils.enums.SpecialChannelPermissions;
import de.nurmarvin.discordbot.utils.throwables.InvalidMentionException;
import de.nurmarvin.discordbot.utils.throwables.NotAMentionException;
import io.sentry.Sentry;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.TextChannel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public class AddSpecialChannelPermissionCommand extends Command {
    public AddSpecialChannelPermissionCommand() {
        super("addspecialchannelpermissions", new String[0], "Add a special permission to a channel", false);
    }

    @Override
    public void execute(String[] args, Message msg) {
        msg.delete().queue();
        EmbedBuilder embedBuilder = ThePalaceBot.getInstance().getEmbedBase();

        if (!PermissionHelper.isAdmin(msg.getMember())) {
            this.sendAutoDeleteMessage(msg.getChannel(), embedBuilder.appendDescription(
                    String.format("You don't seem to have permissions to do this, %s.", msg.getAuthor().getAsMention()))
                                                                     .build());
            return;
        }

        if (args.length < 2) {
            this.sendAutoDeleteMessage(msg.getChannel(),
                                       embedBuilder.appendDescription("Please mention the channel as an argument as " +
                                                                      "well as the special permission you want to add")
                                                   .build());
            return;
        }

        TextChannel textChannel;

        try {
            textChannel = MentionHelper.getChannelFromMention(args[0]);
        } catch (InvalidMentionException | NotAMentionException e) {
            this.sendAutoDeleteMessage(msg.getChannel(),
                                       embedBuilder.appendDescription("Please make sure you actually mention a " +
                                                                      "channel.").build());
            return;
        }

        ArrayList<SpecialChannelPermissions> channelPermissions =
                SpecialChannelUtils.getSpecialPermissions(textChannel.getIdLong());

        for(int i = 1; i < args.length; i++) {
            SpecialChannelPermissions specialChannelPermissions = SpecialChannelPermissions.get(args[i]);
            if(channelPermissions.contains(specialChannelPermissions)) continue;
            channelPermissions.add(specialChannelPermissions);
        }

        SpecialChannelUtils.addSpecialPermissions(textChannel.getIdLong(),
                                                  channelPermissions.toArray(new SpecialChannelPermissions[0]));

        this.sendAutoDeleteMessage(msg.getChannel(),
                                   embedBuilder.appendDescription(
                                           String.format("Added the following special permissions to %s: %s",
                                                         textChannel.getAsMention(),
                                                         Arrays.toString(channelPermissions.toArray()))).build());

        try {
            ThePalaceBot.getInstance().saveConfig();
        } catch (IOException e) {
            Sentry.capture(e);
            this.sendAutoDeleteMessage(msg.getChannel(),
                                       embedBuilder.appendDescription("Error:" + Arrays.toString(e.getStackTrace())).build());
        }
    }
}
