package de.nurmarvin.discordbot.handler;

import de.nurmarvin.discordbot.ThePalaceBot;
import de.nurmarvin.discordbot.utils.CachedMessage;
import io.sentry.Sentry;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.events.guild.GuildBanEvent;
import net.dv8tion.jda.core.events.guild.GuildUnbanEvent;
import net.dv8tion.jda.core.events.message.MessageDeleteEvent;
import net.dv8tion.jda.core.events.message.guild.GuildMessageDeleteEvent;
import net.dv8tion.jda.core.events.message.guild.GuildMessageUpdateEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

public class LogHandler extends ListenerAdapter {
    @Override
    public void onGuildBan(GuildBanEvent event) {
        event.getGuild().getBan(event.getUser()).queue(ban -> {
            EmbedBuilder banLog = ThePalaceBot.getInstance().getLogBase().setTitle("User Banned");

            banLog.addField("User", event.getUser().getAsMention(), false);
            banLog.addField("Reason", ban.getReason(), false);

            ThePalaceBot.getInstance().log(event.getGuild(), banLog.build());
        }, Sentry::capture);
    }

    @Override
    public void onGuildUnban(GuildUnbanEvent event) {
        EmbedBuilder unbanLog = ThePalaceBot.getInstance().getLogBase().setTitle("User Unbanned");

        unbanLog.addField("User", event.getUser().getAsMention(), false);

        ThePalaceBot.getInstance().log(event.getGuild(), unbanLog.build());
    }

    @Override
    public void onGuildMessageDelete(GuildMessageDeleteEvent event) {
        CachedMessage cachedMessage =
                ThePalaceBot.getInstance().getMessageHandler().getMessageCache().get(event.getMessageIdLong());

        if (cachedMessage != null) {
            if (cachedMessage.getOriginalMessage().getAuthor().isBot()) return;
            EmbedBuilder embedBuilder = ThePalaceBot.getInstance().getLogBase().setTitle("Messaged Deleted");

            embedBuilder.addField("User", cachedMessage.getOriginalMessage().getAuthor().getAsMention(), false);
            if (cachedMessage.isEdited()) {
                embedBuilder.addField("Original Message", cachedMessage.getOriginalMessage().getContentRaw(), false);
                embedBuilder.addField("Latest Edit before Deletion", cachedMessage.getLatestEdit().getContentRaw(),
                                      false);
            } else {
                embedBuilder.addField("Message", cachedMessage.getOriginalMessage().getContentRaw(), false);
            }

            embedBuilder.addField("Channel", cachedMessage.getOriginalMessage().getTextChannel().getAsMention(), false);

            if (cachedMessage.getOriginalMessage().getAttachments().size() > 0) {
                embedBuilder.setImage(cachedMessage.getOriginalMessage().getAttachments().get(0).getUrl());
            }

            ThePalaceBot.getInstance().log(event.getGuild(), embedBuilder.build());
        }
    }

    @Override
    public void onGuildMessageUpdate(GuildMessageUpdateEvent event) {
        CachedMessage cachedMessage =
                ThePalaceBot.getInstance().getMessageHandler().getMessageCache().get(event.getMessageIdLong());

        if (cachedMessage != null) {
            if (cachedMessage.getOriginalMessage().getAuthor().isBot()) return;
            EmbedBuilder embedBuilder = ThePalaceBot.getInstance().getLogBase().setTitle("Messaged Edited");

            embedBuilder.addField("User", cachedMessage.getOriginalMessage().getAuthor().getAsMention(), false);
            embedBuilder.addField("Original Message", cachedMessage.getOriginalMessage().getContentRaw(), false);
            embedBuilder.addField("Edited Message", cachedMessage.getLatestEdit().getContentRaw(),
                                  false);

            if (cachedMessage.getOriginalMessage().getAttachments().size() > 0) {
                embedBuilder.setImage(cachedMessage.getOriginalMessage().getAttachments().get(0).getUrl());
            }
            ThePalaceBot.getInstance().log(event.getGuild(), embedBuilder.build());
        }
    }
}
