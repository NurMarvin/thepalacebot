package de.nurmarvin.discordbot.handler;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Maps;
import com.google.gson.reflect.TypeToken;
import de.nurmarvin.discordbot.ThePalaceBot;
import de.nurmarvin.discordbot.utils.*;
import de.nurmarvin.discordbot.utils.enums.SpecialChannelPermissions;
import io.sentry.Sentry;
import lombok.Getter;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.core.events.message.guild.GuildMessageUpdateEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public final class MessageHandler extends ListenerAdapter {

    private static final List<String> OFFENSIVE_WORDS = ImmutableList.of("nigger");
    private HashMap<Long, UserInfo> userInfo;
    private final File userInfoFile;
    @Getter
    private HashMap<Long, CachedMessage> messageCache;

    public MessageHandler() throws IOException {
        this.messageCache = Maps.newHashMap();
        this.userInfoFile = new File("userInfo.json");
        if (this.userInfoFile.exists())
            this.load();
        else {
            this.userInfo = Maps.newHashMap();
            this.save();
        }
    }

    @Override
    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {
        EmbedBuilder embedBuilder = ThePalaceBot.getInstance().getEmbedBase();

        System.out.printf("[MESSAGE] [%s] [%s #%s] %s: %s" + System.lineSeparator(),
                          new SimpleDateFormat("HH:mm:ss").format(new Date()),
                          event.getGuild().getName(),
                          event.getChannel().getName(),
                          event.getAuthor().getName(),
                          event.getMessage().getContentRaw());

        this.messageCache.put(event.getMessageIdLong(), new CachedMessage(event.getMessage()));

        if(checkForInvites(event.getMessage())) return;
        if(checkForLinks(event.getMessage())) return;
        if(ThePalaceBot.getInstance().getCommandRegistry().handleCommand(event)) return;
        if(checkForOffensiveWords(event.getMessage())) return;
        if(event.getAuthor().isBot()) return;
        if(SpecialChannelUtils.getSpecialPermissions(event.getMessage().getChannel().getIdLong()).contains(
                SpecialChannelPermissions.SPAM)) return;

        UserInfo userInfo = this.getUserInfo(event.getAuthor().getIdLong());

        if(userInfo.getLastMessageSent() == 0L || (System.currentTimeMillis() - userInfo.getLastMessageSent()) >= 60000) {
            if(userInfo.addXP()) {

                embedBuilder.setTitle("Level Up");
                embedBuilder.appendDescription(String.format("You've reached Level %s %s %s", userInfo.getLevel(),
                                                             event.getGuild().getEmoteById(537078229069070432L).getAsMention(), event.getMember().getAsMention()));

                if(userInfo.getLevel() % 10 == 0 || userInfo.getLevel() == 5) {
                    embedBuilder.appendDescription("\n\nYou have just unlocked new features \uD83C\uDF89\n\n");
                }

                switch (userInfo.getLevel()) {
                    case 5: {
                        embedBuilder.appendDescription("- External emotes \n");
                        embedBuilder.appendDescription(String.format("- Cool color roles from %s",
                                                                     event.getGuild().getTextChannelById(ThePalaceBot.getInstance().getConfig().getColorPickChannelId()).getAsMention()));
                        event.getGuild().getController().addSingleRoleToMember(event.getMember(),
                                                                               event.getGuild().getRoleById(ThePalaceBot.getInstance().getConfig().getLevelFiveRoleId())).queue();
                        break;
                    }
                    case 10: {
                        embedBuilder.appendDescription(
                                String.format("- A private voice channel, further information in %s",
                                              event.getGuild().getTextChannelById(ThePalaceBot.getInstance().getConfig().getPrivateVoiceChannelExplanationChannelId()).getAsMention()));
                        event.getGuild().getController().addSingleRoleToMember(event.getMember(),
                                                                               event.getGuild().getRoleById(ThePalaceBot.getInstance().getConfig().getLevelTenRoleId())).queue();
                        break;
                    }
                    case 20: {
                        embedBuilder.appendDescription("- Send images into general chat\n");
                        embedBuilder.appendDescription("- Access to the report tool");
                        event.getGuild().getController().addSingleRoleToMember(event.getMember(),
                                                                               event.getGuild().getRoleById(ThePalaceBot.getInstance().getConfig().getLevelTwentyRoleId())).queue();
                        break;
                    }
                    case 30: {
                        embedBuilder.appendDescription("- Post links into chat (with embeds)\n");
                        embedBuilder.appendDescription("- You are now shown higher in the server list");
                        event.getGuild().getController().addSingleRoleToMember(event.getMember(),
                                                                               event.getGuild().getRoleById(ThePalaceBot.getInstance().getConfig().getLevelThirtyRoleId())).queue();
                        break;
                    }
                    case 40: {
                        embedBuilder.appendDescription(
                                String.format("- You can now get your own custom role, further information in %s",
                                              event.getGuild().getTextChannelById(ThePalaceBot.getInstance().getConfig().getSpecialRoleChannelId()).getAsMention()));
                        event.getGuild().getController().addSingleRoleToMember(event.getMember(),
                                                                               event.getGuild().getRoleById(ThePalaceBot.getInstance().getConfig().getLevelFortyRoleId())).queue();
                        break;
                    }
                }

                ThePalaceBot.getInstance().sendAutoDeleteMessage(event.getChannel(), embedBuilder.build(), 15);
            }

            userInfo.setLastMessageSent(System.currentTimeMillis());

            try {
                this.save();
            } catch (IOException e) {
                Sentry.capture(e);
                e.printStackTrace();
            }
        }
    }

    public UserInfo getUserInfo(long userId) {
        if(this.userInfo.containsKey(userId)) return this.userInfo.get(userId);

        UserInfo userInfo = new UserInfo(userId, 0L, 0, 0L, 0L);
        this.userInfo.put(userId, userInfo);

        try {
            this.save();
        } catch (IOException e) {
            Sentry.capture(e);
            e.printStackTrace();
        }

        return userInfo;
    }

    @Override
    public void onGuildMessageUpdate(GuildMessageUpdateEvent event) {
        System.out.printf("[EDIT] [%s] [%s #%s] %s: %s" + System.lineSeparator(),
                          new SimpleDateFormat("HH:mm:ss").format(new Date()),
                          event.getGuild().getName(),
                          event.getChannel().getName(),
                          event.getAuthor().getName(),
                          event.getMessage().getContentRaw());

        if(messageCache.containsKey(event.getMessageIdLong())) {

            CachedMessage cachedMessage = this.messageCache.get(event.getMessageIdLong());

            cachedMessage.setLatestEdit(event.getMessage());

            this.messageCache.put(event.getMessageIdLong(), cachedMessage);
        }

        checkForLinks(event.getMessage());
        checkForInvites(event.getMessage());
        checkForOffensiveWords(event.getMessage());
    }

    private boolean checkForInvites(Message message) {
        if(message.getContentRaw().toLowerCase().contains("discord.gg/") && !PermissionHelper.isMod(message.getMember())) {
            message.delete().queue();
            ThePalaceBot.getInstance().sendAutoDeleteMessage(message.getChannel(),
                                                             ThePalaceBot.getInstance().getEmbedBase().appendDescription(
                                                                     "Please don't send invite links!").build(), 5);
            return true;
        }
        return false;
    }

    private boolean checkForLinks(Message message) {
        UserInfo userInfo = this.getUserInfo(message.getAuthor().getIdLong());
        if(LinkHelper.containsLink(message.getContentRaw()) && userInfo.getLevel() < 30 && !PermissionHelper.isMod(message.getMember()) && !SpecialChannelUtils.getSpecialPermissions(message.getChannel().getIdLong()).contains(SpecialChannelPermissions.LINKS)) {
            message.delete().queue();
            ThePalaceBot.getInstance().sendAutoDeleteMessage(message.getChannel(),
                                                             ThePalaceBot.getInstance().getEmbedBase().appendDescription(
                                                                     "You're not allowed to send links until you reach level 30.").build(), 5);
            return true;
        }
        return false;
    }

    private boolean checkForMassPing(Message message) {
        if(message.getMentionedUsers().size() > 10) {
            message.delete().queue();
            message.getGuild().getController().kick(message.getMember(), "Mass Mention (>10 individual Users)").queue(success -> {
                EmbedBuilder embedBuilder = ThePalaceBot.getInstance().getLogBase();

                embedBuilder.addField("Offender", message.getAuthor().getAsMention(), false);
                embedBuilder.addField("AutoMod", "Mass Mention", false);
                embedBuilder.addField("Action Taken", "Kick", false);

                ThePalaceBot.getInstance().log(message.getGuild(), embedBuilder.build());
            }, Sentry::capture);
            return true;
        }
        return false;
    }

    private boolean checkForOffensiveWords(Message message) {
        if (message.getAuthor().isBot() && !message.getJDA().getSelfUser().equals(message.getAuthor())) {
            return false;
        }

        if (message.getAuthor().equals(message.getJDA().getSelfUser()))
            return false;

        String messageConverted = message.getContentRaw()
                                         .toLowerCase()
                                         .replace(" ", "")
                                         .replace("-", "")
                                         .replace(".", "")
                                         .replace(" ", "")
                                         .replace("1", "i")
                                         .replace("3", "e")
                                         .replace("a", "e")
                                         .replace("l", "i")
                                         .replace("|", "i")
                                         .replace("!", "i")
                                         .replace("¡", "i");

        for(String word : OFFENSIVE_WORDS) {
            if (messageConverted.contains(word) &&
                !PermissionHelper.canBypass(message.getMember())) {
                EmbedBuilder embedBuilder = ThePalaceBot.getInstance().getEmbedBase();

                embedBuilder
                        .appendDescription(
                                String.format("The use of the N word with a hard R is strictly prohibited.\n" +
                                              "Please note that multiple offenses will lead to a mute. %s",
                                              message.getAuthor().getAsMention()));

                ThePalaceBot.getInstance().sendAutoDeleteMessage(message.getChannel(), embedBuilder.build(), 10);

                message.delete().queue();
                return true;
            }
        }
        return false;
    }

    private void load() throws FileNotFoundException {
        this.userInfo = ThePalaceBot.getInstance().getGson().fromJson(new FileReader(this.userInfoFile),
                                                                      new TypeToken<HashMap<Long,
                                                                              UserInfo>>() {}
                                                                              .getType());
    }

    public void save() throws IOException {
        FileWriter fileWriter = new FileWriter(this.userInfoFile);
        fileWriter.write(ThePalaceBot.getInstance().getGson().toJson(this.userInfo));
        fileWriter.close();
    }
}