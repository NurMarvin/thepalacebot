package de.nurmarvin.discordbot.handler;

import de.nurmarvin.discordbot.ThePalaceBot;
import de.nurmarvin.discordbot.utils.*;
import de.nurmarvin.discordbot.utils.enums.SpecialChannelPermissions;
import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

import java.util.ArrayList;

public class SpecialChannelHandler extends ListenerAdapter {

    @Override
    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {

        if(event.getAuthor().equals(event.getJDA().getSelfUser()))
            return;

        ArrayList<SpecialChannelPermissions> channelPermissions =
                SpecialChannelUtils.getSpecialPermissions(event.getChannel().getIdLong());

        if(!PermissionHelper.canBypass(event.getMember())) {
            if(channelPermissions.contains(SpecialChannelPermissions.IMAGES)
               && !AttachmentHelper.hasImageAttached(event.getMessage())) {

                event.getMessage().delete().queue();

                EmbedBuilder embedBuilder = this.getWarningEmbedBuilder();

                embedBuilder.appendDescription(String.format("Please only post images in here, any conversations " +
                                                             "about these images should be kept in the " +
                                                             "respective channels for it %s",
                                                             event.getAuthor().getAsMention()));

                ThePalaceBot.getInstance().sendAutoDeleteMessage(event.getChannel(), embedBuilder.build(), 5);
            } else if(channelPermissions.contains(SpecialChannelPermissions.BOT_COMMANDS)
                       && !BotCommandHelper.isBotCommand(event.getMessage().getContentRaw())) {

                event.getMessage().delete().queue();

                EmbedBuilder embedBuilder = this.getWarningEmbedBuilder();

                embedBuilder.appendDescription(String.format("Please only post bot commands in this channel %s",
                                                             event.getAuthor().getAsMention()));

                ThePalaceBot.getInstance().sendAutoDeleteMessage(event.getChannel(), embedBuilder.build(), 5);
            }
        }
    }

    private EmbedBuilder getWarningEmbedBuilder() {
        EmbedBuilder embedBuilder = ThePalaceBot.getInstance().getEmbedBase();

        embedBuilder.setTitle("Warning");

        return embedBuilder;
    }
}
