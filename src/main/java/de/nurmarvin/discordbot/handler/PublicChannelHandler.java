package de.nurmarvin.discordbot.handler;

import de.nurmarvin.discordbot.ThePalaceBot;
import io.sentry.Sentry;
import net.dv8tion.jda.core.entities.Category;
import net.dv8tion.jda.core.entities.VoiceChannel;
import net.dv8tion.jda.core.events.guild.voice.GuildVoiceJoinEvent;
import net.dv8tion.jda.core.events.guild.voice.GuildVoiceLeaveEvent;
import net.dv8tion.jda.core.events.guild.voice.GuildVoiceUpdateEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

public class PublicChannelHandler extends ListenerAdapter {
    @Override
    public void onGuildVoiceJoin(GuildVoiceJoinEvent event) {
        copyIfVoiceChannelNeeded(event.getChannelJoined());
    }

    @Override
    public void onGuildVoiceUpdate(GuildVoiceUpdateEvent event) {
        try {
            if (event.getVoiceState().inVoiceChannel()) {
                this.copyIfVoiceChannelNeeded(event.getVoiceState().getChannel());
                this.checkIfEmptyChannels(event.getVoiceState().getChannel());
            }

            this.copyIfVoiceChannelNeeded(event.getChannelLeft());
        }
        catch (Exception e) {
            Sentry.capture(e);
        }
    }

    @Override
    public void onGuildVoiceLeave(GuildVoiceLeaveEvent event) {
        this.checkIfEmptyChannels(event.getChannelLeft());
    }

    private void checkIfEmptyChannels(VoiceChannel voiceChannel) {
        if(voiceChannel.getParent() != null) {
            if (voiceChannel.getParent().getIdLong() == ThePalaceBot.getInstance().getConfig().getPublicVoiceChannelCategoryId()) {
                int newId = 1;
                for (int i = 0; i < voiceChannel.getParent().getVoiceChannels().size(); i++) {
                    if(voiceChannel.getParent().getVoiceChannels().get(0).getMembers().size() > 0) continue;
                    VoiceChannel channels = voiceChannel.getParent().getVoiceChannels().get(i);

                    channels.getManager().setName("Public #" + newId).queue();

                    int id = Integer.valueOf(channels.getName().split("#")[1]);
                    if (channels.getMembers().size() == 0 && id != 1) {
                        channels.delete().queue();
                    } else {
                        newId++;
                    }
                }
            }
        }
    }

    private void copyIfVoiceChannelNeeded(VoiceChannel voiceChannel) {
        if(voiceChannel.getParent() != null) {
            if(voiceChannel.getParent().getIdLong() == ThePalaceBot.getInstance().getConfig().getPublicVoiceChannelCategoryId()) {
                if(voiceChannel.getParent().getVoiceChannels().size() > 1)
                    for (VoiceChannel channels : voiceChannel.getParent().getVoiceChannels())
                        if (channels.getMembers().size() == 0) return;
                if(voiceChannel.getMembers().size() == 1) {
                    int id = this.getHighestId(voiceChannel.getParent()) + 1;
                    voiceChannel.createCopy().setPosition(null).setName(voiceChannel.getName().substring(0,
                                                                                       voiceChannel.getName().length() - 1) + id).queue(ignored -> {});
                }
            }
        }
    }

    public int getHighestId(Category category) {
        int highestId = 0;

        for (VoiceChannel voiceChannel : category.getVoiceChannels()) {
            int id = Integer.valueOf(voiceChannel.getName().split("#")[1]);

            if(highestId + 1 != id) return highestId;

            if(id > highestId) highestId = id;
        }

        return highestId;
    }
}
